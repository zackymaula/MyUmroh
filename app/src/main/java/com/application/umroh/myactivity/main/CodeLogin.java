package com.application.umroh.myactivity.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.application.umroh.R;
import com.application.umroh.myactivity.main.kantorpusat.CodeKPInbox;
import com.application.umroh.myactivity.main.leader.CodeLeadTerkirim;

public class CodeLogin extends AppCompatActivity {

    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        btnLogin = (Button)findViewById(R.id.button_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), CodeKPInbox.class));
            }
        });

        btnLogin.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startActivity(new Intent(getBaseContext(), CodeLeadTerkirim.class));
                return false;
            }
        });
    }
}
